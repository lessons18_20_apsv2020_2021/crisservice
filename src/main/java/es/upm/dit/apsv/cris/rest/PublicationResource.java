package es.upm.dit.apsv.cris.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.upm.dit.apsv.cris.dao.PublicationDAOImplementation;
import es.upm.dit.apsv.cris.model.Publication;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


@Path("/Publications") //Path to get here: CRISSERVICE + rest + Publication
public class PublicationResource {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Publication> readAll () { //java method we are linking to the http method
	        return PublicationDAOImplementation.getInstance().readAll();
	}
	//The resulting java objec (list of publications) will be converted to a textual representation in jason
	//that would be the http response to the get

	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(Publication pnew) throws URISyntaxException {
		Publication p = PublicationDAOImplementation.getInstance().create(pnew);
	    URI uri = new URI("/CRISSERVICE/rest/Publications/" + p.getId()); //Creates a uri so the client can access to this resource
	    return Response.created(uri).build();
	}
	
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response read(@PathParam("id") String id) {//Produces full representation of the Publication
		Publication p = PublicationDAOImplementation.getInstance().read(id);
	    if (p == null)
	      return Response.status(Response.Status.NOT_FOUND).build();
	    return Response.ok(p, MediaType.APPLICATION_JSON).build();
	}        

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("{id}")
	public Response update(@PathParam("id") String id, Publication p) {
		Publication pold = PublicationDAOImplementation.getInstance().read(id);
	    if ((pold == null) || (! pold.getId().contentEquals(p.getId())))
	      return Response.notModified().build();
	    PublicationDAOImplementation.getInstance().update(p);
	    return Response.ok().build();//Only ok if the objecto was already in the DB                
	}
	   
	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") String  id) {
		Publication pold = PublicationDAOImplementation.getInstance().read(id);
	    if (pold == null)
	        return Response.notModified().build();
	    PublicationDAOImplementation.getInstance().delete(pold);
	    return Response.ok().build();
	}	        
}
