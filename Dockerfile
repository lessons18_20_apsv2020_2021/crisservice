FROM tomcat:9-jdk8-openjdk

ADD target/CRISSERVICE-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/CRISSERVICE.war

ADD crisservice_start.sh .

ADD .m2/repository/com/h2database/h2/1.4.197/h2-1.4.197.jar .

EXPOSE 8080

CMD ["sh", "crisservice_start.sh"]